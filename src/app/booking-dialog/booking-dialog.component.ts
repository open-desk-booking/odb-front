import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Desk, DeskSelect, DialogData} from "../../models/booking";
import {BookingService} from "../../services/booking.service";
import {LocalStorageService} from "../../services/local-storage.service";

@Component({
  selector: 'app-booking-dialog',
  templateUrl: './booking-dialog.component.html',
  styleUrls: ['./booking-dialog.component.scss'],
})
export class BookingDialogComponent implements OnInit {
  desks: DeskSelect[] = []

  constructor(
    private bookingService: BookingService,
    public dialogRef: MatDialogRef<BookingDialogComponent>,
    private localStorageService: LocalStorageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
  }

  onNoClick(): void {
    this.localStorageService.setData('name', this.data.name)
    this.localStorageService.setData('deskId', this.data.deskId)
    this.dialogRef.close();
  }

  onYesClick(): void {
    this.localStorageService.setData('name', this.data.name)
    this.localStorageService.setData('deskId', this.data.deskId)
    this.dialogRef.close(this.data);
  }

  ngOnInit(): void {
    const toDeskSelect = (desk: Desk) => ({
      ...desk,
      isAvailable: !this.data.desksNotAvailable.includes(desk.id)
    } as DeskSelect)

    this.bookingService
      .getDesks()
      .subscribe((desks: Desk[]) => this.desks = desks.map(toDeskSelect));
    this.data.name = this.localStorageService.getData('name');
    this.data.deskId = this.localStorageService.getData('deskId');
  }
}
