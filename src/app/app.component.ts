import {Component, OnInit} from '@angular/core';
import {BookingService} from "../services/booking.service";
import {Day, DialogData} from "../models/booking";
import {BehaviorSubject, concatMap, filter} from "rxjs";
import {MatDialog} from "@angular/material/dialog";
import {BookingDialogComponent} from "./booking-dialog/booking-dialog.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Open Desk Booking tool';
  bookingsGroupByDay: Day[] = [];
  selectedDay$: BehaviorSubject<Date> = new BehaviorSubject<Date>(new Date(Date.now()));

  constructor(
    private bookingService: BookingService,
    public dialog: MatDialog,
  ) {
  }

  updateBookingsGroupByDay = (bookings: Day[]) => this.bookingsGroupByDay = bookings;

  ngOnInit(): void {
    this.selectedDay$
      .pipe(concatMap((selectedDay: Date) => this.bookingService
        .getWeekWith(selectedDay.toISOString().slice(0, 10))))
      .subscribe(this.updateBookingsGroupByDay)
  }

  openDialog(day: string) {
    const isEverythingNotNull = (booking: DialogData) => !!booking && !!booking.day && !!booking.deskId && !!booking.name;
    const getReservedDesks = (day: string) => this.bookingsGroupByDay
      .find(booking => booking.day == day)
      ?.bookings.map(booking => booking.desk?.data.id)

    this.dialog
      .open(BookingDialogComponent, {data: {day: day, desksNotAvailable: getReservedDesks(day)}})
      .afterClosed()
      .pipe(
        filter(isEverythingNotNull),
        concatMap((booking: any) => this.bookingService.createBooking(booking.day, booking.name, booking.deskId)),
        concatMap(_ => this.bookingService.getWeekWith(day)))
      .subscribe(this.updateBookingsGroupByDay)
  }

  previousWeek(): void {
    const selectedDay = this.selectedDay$.value
    const previousWeekDay = new Date(selectedDay.setDate(selectedDay.getDate() - 7))
    this.selectedDay$.next(previousWeekDay)
  }

  nextWeek(): void {
    const selectedDay = this.selectedDay$.value
    const nextWeekDay = new Date(selectedDay.setDate(selectedDay.getDate() + 7))
    this.selectedDay$.next(nextWeekDay)
  }
}
