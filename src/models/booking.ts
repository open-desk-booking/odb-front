import {StrapiSingleResponse} from "./strapiResponse";

export class Booking {
  name: string
  day: string
  desk?: StrapiSingleResponse<Desk>

  constructor(
    name: string,
    day: string,
    desk?: StrapiSingleResponse<Desk>,
  ) {
    this.name = name;
    this.day = day;
    this.desk = desk;
  }
}

export interface Desk {
  id: number
  name: string
  createdAt?: string
  updatedAt?: string
  publishedAt?: string
  slug: string
}

export interface DeskSelect extends Desk{
  isAvailable: boolean
}

export interface Day {
  day: string,
  bookings: Booking[]
}

export interface DialogData {
  day: string,
  deskId?: number,
  name?: string,
  desksNotAvailable: number[],
}
