export interface StrapiResponse<T> {
  data: Entry<T>[];
  meta: {
    pagination?: {
      "page"?: number,
      "pageSize"?: number,
      "pageCount"?: number,
      "total"?: number
    }
  };
}

export interface StrapiSingleResponse<T> {
  data: Entry<T>;
  meta: {};
}

export interface Entry<T> {
  id: number;
  attributes: T;
}
