import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Entry, StrapiResponse} from "../models/strapiResponse";
import {map, Observable, zip} from "rxjs";
import {Booking, Day, Desk} from "../models/booking";

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  baseUrl = 'http://localhost:1337/api'

  constructor(private http: HttpClient) {
  }

  private mapAttributes = (elem: Entry<any>) => elem.attributes;

  getDay(day: string): Observable<Booking[]> {
    return this.http
      .get<StrapiResponse<Booking>>(`${this.baseUrl}/bookings?populate=*&&filters[day][$eq]=${day}`)
      .pipe(map(bookings => bookings.data.map(this.mapAttributes)));
  }

  getWeekWith(day: string): Observable<Day[]> {
    const addDay = (date: Date, day: number) => new Date(date.setDate(date.getDate() + day)).toISOString().slice(0, 10);

    const _day = new Date(day);
    const _dayNumInTheWeek = _day.getDay() - 1;
    const _day1 = addDay(new Date(_day), 0 - _dayNumInTheWeek)
    const _day2 = addDay(new Date(_day), 1 - _dayNumInTheWeek)
    const _day3 = addDay(new Date(_day), 2 - _dayNumInTheWeek)
    const _day4 = addDay(new Date(_day), 3 - _dayNumInTheWeek)
    const _day5 = addDay(new Date(_day), 4 - _dayNumInTheWeek)

    return zip(
      this.getDay(_day1),
      this.getDay(_day2),
      this.getDay(_day3),
      this.getDay(_day4),
      this.getDay(_day5)
    )
      .pipe(map(([day1, day2, day3, day4, day5]) => [
        {day: _day1, bookings: [...day1]},
        {day: _day2, bookings: [...day2]},
        {day: _day3, bookings: [...day3]},
        {day: _day4, bookings: [...day4]},
        {day: _day5, bookings: [...day5]}
      ]))
  }

  createBooking(day: string, name: string, desk: number): Observable<Booking> {
    return this.http
      .post<Booking>(`${this.baseUrl}/bookings`, {data: {day, name, desk}})
  }

  getDesks(): Observable<Desk[]> {
    return this.http
      .get<StrapiResponse<Desk>>(`${this.baseUrl}/desks?pagination[pageSize]=30&sort=slug`)
      .pipe(map((desks: StrapiResponse<Desk>) => desks.data.map((d: Entry<Desk>) => ({
        id: d.id,
        slug: d.attributes.slug,
        name: d.attributes.name
      }))))
  }
}
